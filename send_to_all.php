<?php

require_once('configuration.php');
require_once('vendor/autoload.php');

use dott_xado\TelegramBot\Database;

$db = Database::getInstance();

$sql = 'LISTEN "messageAdded"';

$result = $db->exec($sql);

while (1) {
	$result = "";
	// wait for one Notify 10 seconds instead of using sleep(10)
	$result = $db->pgsqlGetNotify(10000); 

	if ( $result ) { 
		$search = $result['payload'];
		$search = json_decode($search);
		new dott_xado\TelegramBot\SendToAll($search);
	}

	flush();
}