CREATE DATABASE XXXXXX
  WITH OWNER = kerika
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'C.UTF-8'
       LC_CTYPE = 'C.UTF-8'
       CONNECTION LIMIT = -1;



CREATE SEQUENCE public.langs_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.langs_seq
  OWNER TO kerika;

  CREATE SEQUENCE public.telegram_responses_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.telegram_responses_seq
  OWNER TO kerika;

CREATE SEQUENCE public.telegram_updates_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.telegram_updates_seq
  OWNER TO kerika;


CREATE SEQUENCE public.texts_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.texts_seq
  OWNER TO kerika;

CREATE SEQUENCE public.exceptions_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.exceptions_id_seq
  OWNER TO kerika;

CREATE SEQUENCE public.message_to_all_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.message_to_all_id_seq
  OWNER TO kerika;


### -------------------------------------------------


CREATE TABLE public.chats
(
  id bigint NOT NULL,
  type character varying(50) NOT NULL DEFAULT ''::character varying,
  title character varying(200) DEFAULT NULL::character varying,
  username character varying(200) DEFAULT NULL::character varying,
  first_name character varying(200) DEFAULT NULL::character varying,
  last_name character varying(200) DEFAULT NULL::character varying,
  all_members_are_administrators smallint,
  is_active smallint NOT NULL DEFAULT '1'::smallint,
  insert_date timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  CONSTRAINT chats_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.chats
  OWNER TO kerika;


CREATE TABLE public.langs
(
  id integer NOT NULL DEFAULT nextval('langs_seq'::regclass),
  code character varying(2) NOT NULL DEFAULT ''::character varying,
  is_default smallint NOT NULL DEFAULT '0'::smallint,
  CONSTRAINT langs_pkey PRIMARY KEY (id),
  CONSTRAINT langs_id_check CHECK (id > 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.langs
  OWNER TO kerika;


CREATE TABLE public.telegram_responses
(
  id integer NOT NULL DEFAULT nextval('telegram_responses_seq'::regclass),
  ok smallint NOT NULL,
  error_code character varying(3) DEFAULT NULL::character varying,
  description text,
  result json,
  insert_data timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT telegram_responses_pkey PRIMARY KEY (id),
  CONSTRAINT telegram_responses_id_check CHECK (id > 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.telegram_responses
  OWNER TO kerika;


CREATE TABLE public.telegram_updates
(
  id integer NOT NULL DEFAULT nextval('telegram_updates_seq'::regclass),
  content json,
  insert_data timestamp(0) without time zone DEFAULT now(),
  CONSTRAINT telegram_updates_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.telegram_updates
  OWNER TO kerika;


CREATE TABLE public.texts
(
  id integer NOT NULL DEFAULT nextval('texts_seq'::regclass),
  label character varying(50) NOT NULL DEFAULT ''::character varying,
  language_code character varying(3) NOT NULL DEFAULT ''::character varying,
  text text NOT NULL,
  CONSTRAINT texts_pkey PRIMARY KEY (id),
  CONSTRAINT texts_id_check CHECK (id > 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.texts
  OWNER TO kerika;


CREATE TABLE public.users
(
  id integer NOT NULL,
  first_name character varying(100) DEFAULT ''::character varying,
  last_name character varying(100) DEFAULT ''::character varying,
  username character varying(100) DEFAULT NULL::character varying,
  language_code character varying(10) NOT NULL DEFAULT ''::character varying,
  insert_date timestamp(0) without time zone NOT NULL DEFAULT now(),
  is_administrator smallint NOT NULL DEFAULT '0'::smallint,
  CONSTRAINT users_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.users
  OWNER TO kerika;

CREATE TABLE public.exceptions
(
  id integer NOT NULL DEFAULT nextval('exceptions_id_seq'::regclass),
  message text NOT NULL,
  insert_data timestamp with time zone NOT NULL DEFAULT now(),
  telegram_updates_id integer,
  CONSTRAINT exceptions_id PRIMARY KEY (id),
  CONSTRAINT telegram_updates_exceptions FOREIGN KEY (telegram_updates_id)
      REFERENCES public.telegram_updates (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.exceptions
  OWNER TO kerika;



CREATE TABLE public.message_to_all
(
  id integer NOT NULL DEFAULT nextval('message_to_all_id_seq'::regclass),
  message text NOT NULL,
  insert_data timestamp with time zone NOT NULL DEFAULT now(),
  sent_data timestamp with time zone,
  chat_id integer NOT NULL,
  CONSTRAINT messages_to_all_id PRIMARY KEY (id),
  CONSTRAINT chat_messages_to_all FOREIGN KEY (chat_id)
      REFERENCES public.chats (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.message_to_all
  OWNER TO kerika;


###------------------------------

CREATE OR REPLACE FUNCTION public.sendtoall()
  RETURNS trigger AS
$BODY$
BEGIN
  PERFORM pg_notify('messageAdded', row_to_json(NEW)::text);
  RETURN new;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sendtoall()
  OWNER TO kerika;

CREATE TRIGGER message_to_all_after
  AFTER INSERT
  ON public.message_to_all
  FOR EACH ROW
  EXECUTE PROCEDURE public.sendtoall();

