<?php

use dott_xado\Bot\Bot;

require_once('configuration.php');
require_once('vendor/autoload.php');

$content = file_get_contents("php://input");
$update_raw = json_decode($content, true);

if (!$update_raw) {
  // receive wrong update, must not happen
  exit;
}

$bot = new Bot($update_raw);

$bot->handleUpdate();